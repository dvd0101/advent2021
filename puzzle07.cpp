#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <optional>
#include <ranges>
#include <sstream>
#include <vector>

namespace ranges = std::ranges;

class board {
  public:
    void add_number(int v);
    bool mark(int v);
    bool winning() const;
    size_t size() const;
    uint64_t score() const;

  private:
  public:
    std::vector<int> nums;
};

struct boards {
    bool mark(int v);
    std::optional<board> winning() const;
    void discard_all_winning_boards();

    std::vector<board> boards;
};

std::vector<int> parse_numbers(std::string line);
void insert_number(boards& b, int num);

int main() {
    std::ifstream in("./inputs/input7");

    std::string num_line;
    std::getline(in, num_line);

    std::vector<int> numbers = parse_numbers(num_line);

    boards b;
    int x;
    while (in >> x)
        insert_number(b, x);

    uint64_t first_winning_score = 0;
    uint64_t last_winning_score = 0;
    for (auto input : numbers) {
        if (!b.mark(input))
            continue;

        if (auto w = b.winning(); w) {
            if (first_winning_score == 0)
                first_winning_score = w->score() * input;
            else
                last_winning_score = w->score() * input;
            b.discard_all_winning_boards();
        }
    }

    std::cout << ">>1 " << first_winning_score << "\n";
    std::cout << ">>2 " << last_winning_score << "\n";
}

struct comma_whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |= space;
        return &v[0];
    }
    comma_whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

std::vector<int> parse_numbers(std::string line) {
    using namespace std;

    istringstream in(move(line));
    in.imbue(std::locale(in.getloc(), new comma_whitespace));
    vector<int> numbers;

    copy(istream_iterator<int>(in), istream_iterator<int>(), back_inserter(numbers));
    return numbers;
}

void insert_number(boards& b, int num) {
    bool needs_new = b.boards.size() == 0 || b.boards.back().size() == 25;
    if (needs_new)
        b.boards.push_back({});
    b.boards.back().add_number(num);
}

void board::add_number(int v) {
    assert(nums.size() < 25);
    assert(v >= 0);

    nums.push_back(v);
}

bool board::mark(int v) {
    auto it = ranges::find(nums, v);
    if (it != nums.end()) {
        *it *= -1;
        return true;
    }
    return false;
}

bool board::winning() const {
    for (int row = 0; row < 5; row++) {
        bool win = true;
        for (int col = 0; col < 5 && win; col++)
            win &= nums[row * 5 + col] < 0;
        if (win)
            return true;
    }

    for (int col = 0; col < 5; col++) {
        bool win = true;
        for (int row = 0; row < 5 && win; row++)
            win &= nums[row * 5 + col] < 0;
        if (win)
            return true;
    }

    return false;
}

size_t board::size() const { return nums.size(); }

uint64_t board::score() const {
    auto v = nums | std::views::filter([](int x) { return x > 0; });
    return std::accumulate(v.begin(), v.end(), 0);
}

bool boards::mark(int v) {
    bool found = false;
    for (auto& b : boards)
        found |= b.mark(v);
    return found;
}

void boards::discard_all_winning_boards() {
    bool found;
    do {
        found = false;
        for (size_t i = 0; i < boards.size(); i++) {
            if (boards[i].winning()) {
                found = true;
                boards.erase(boards.begin() + i);
                break;
            }
        }
    } while (found);
}

std::optional<board> boards::winning() const {
    for (auto& b : boards)
        if (b.winning())
            return b;
    return {};
}
