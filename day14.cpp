#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <string_view>
#include <unordered_map>

namespace ranges = std::ranges;

using subs_map = std::unordered_map<std::string, char>;

struct tpl_string {
    std::unordered_map<std::string, uint64_t> keys;
    std::unordered_map<char, uint64_t> elems;
};

std::ostream& operator<<(std::ostream&, const tpl_string&);

std::pair<tpl_string, subs_map> parse(std::istream&);

tpl_string interpolate(const tpl_string& tpl, const subs_map& rules);
uint64_t analyze(const tpl_string& tpl);

int main() {
    std::ifstream in("./inputs/day14");
    auto [tpl, rules] = parse(in);

    for (int i = 0; i < 10; i++) {
        tpl = interpolate(tpl, rules);
    }

    auto result = analyze(tpl);
    std::cout << ">>1 " << result << "\n";

    for (int i = 10; i < 40; i++) {
        tpl = interpolate(tpl, rules);
    }

    result = analyze(tpl);
    std::cout << ">>2 " << result << "\n";
}

std::ostream& operator<<(std::ostream& os, const tpl_string& tpl) {
    for (auto& [k, n] : tpl.keys) {
        os << k << "->" << n << " ";
    }

    for (auto& [c, n] : tpl.elems) {
        os << c << ":" << n << " ";
    }
    return os << "\n";
}

std::pair<tpl_string, subs_map> parse(std::istream& in) {
    std::string tpl;
    subs_map rules;

    in >> tpl;

    std::string a, tmp;
    char b;
    while (in >> a >> tmp >> b)
        rules[a] = b;

    tpl_string t;
    for (size_t i = 0; i < tpl.size() - 1; i++) {
        std::string key(&tpl[i], 2);
        t.keys[key]++;
    }

    for (auto ch : tpl)
        t.elems[ch]++;

    return {t, rules};
}

tpl_string interpolate(const tpl_string& tpl, const subs_map& rules) {
    tpl_string out = tpl;
    for (auto& [key, n] : tpl.keys) {
        if (auto it = rules.find(key); it != rules.end()) {
            std::string nk = key;
            nk[1] = it->second;
            out.keys[nk] += n;

            nk[0] = it->second;
            nk[1] = key[1];
            out.keys[nk] += n;

            out.elems[it->second] += n;
            out.keys[key] -= n;
        }
    }
    return out;
}

uint64_t analyze(const tpl_string& tpl) {
    uint64_t min_el = std::numeric_limits<uint64_t>::max();
    uint64_t max_el = 0;
    for (auto [c, n] : tpl.elems) {
        if (n > max_el) {
            max_el = n;
        }
        if (n < min_el) {
            min_el = n;
        }
    }
    return max_el - min_el;
}
