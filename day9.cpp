#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <ranges>
#include <set>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

using point = std::pair<int32_t, int32_t>;
using basin = std::vector<uint8_t>;

struct height_map {
    int32_t line_size = 0;
    std::vector<uint8_t> points;

    int32_t height() const { return points.size() / line_size; }
    int32_t width() const { return line_size; }

    uint8_t lookup(int32_t x, int32_t y) const {
        if (x < 0 || x >= width() || y < 0 || y >= height())
            return 9;

        return points[y * width() + x];
    }

    uint8_t lookup(point p) const {
        auto [x, y] = p;
        return lookup(x, y);
    }
};

height_map parse(std::istream&);

std::vector<point> search_low_points(const height_map&);

uint32_t risk_level(const height_map& hmap, const std::vector<point>&);

std::vector<basin> search_basins(const height_map&, const std::vector<point>& seeds);

uint32_t basins_sizes(std::vector<basin>);

int main() {
    std::ifstream in("./inputs/day9");
    auto hmap = parse(in);

    auto low_points = search_low_points(hmap);
    std::cout << ">>1 " << risk_level(hmap, low_points) << "\n";

    auto basins = search_basins(hmap, low_points);
    std::cout << ">>2 " << basins_sizes(std::move(basins)) << "\n";
}

height_map parse(std::istream& in) {
    height_map h;
    char c;
    while (in >> c)
        h.points.push_back(c - '0');

    in.clear();
    in.seekg(0);
    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    h.line_size = static_cast<int32_t>(in.tellg()) - 1;

    return h;
}

std::vector<point> search_low_points(const height_map& hmap) {
    std::vector<point> out;

    for (int32_t y = 0; y < hmap.height(); y++) {
        for (int32_t x = 0; x < hmap.width(); x++) {
            auto v = hmap.lookup(x, y);
            if (v < hmap.lookup(x - 1, y) && v < hmap.lookup(x + 1, y) && v < hmap.lookup(x, y - 1)
                && v < hmap.lookup(x, y + 1)) {
                out.push_back({x, y});
            }
        }
    }

    return out;
}

uint32_t risk_level(const height_map& hmap, const std::vector<point>& pts) {
    uint32_t risk = 0;
    for (auto& [x, y] : pts) {
        risk += 1 + hmap.lookup(x, y);
    }
    return risk;
}

basin flood_fill(const height_map& hmap, point p) {
    auto valid_dir = [&](point pt, uint8_t ref) {
        auto v = hmap.lookup(pt);
        return v > ref && v < 9;
    };

    basin out = {hmap.lookup(p)};
    std::set<point> added{p};
    std::vector<std::pair<point, uint8_t>> stack{{p, out.back()}};

    while (!stack.empty()) {
        auto [pt, ref] = stack.back();
        stack.pop_back();

        auto [x, y] = pt;

        std::array<point, 4> search{{{x - 1, y}, {x + 1, y}, {x, y - 1}, {x, y + 1}}};
        for (auto pt : search) {
            if (added.contains(pt) || !valid_dir(pt, ref))
                continue;

            auto nv = hmap.lookup(pt);

            out.push_back(nv);
            added.insert(pt);
            stack.push_back({pt, nv});
        }
    }

    return out;
}

std::vector<basin> search_basins(const height_map& hmap, const std::vector<point>& seeds) {
    std::vector<basin> out;
    for (auto& pt : seeds) {
        out.push_back(flood_fill(hmap, pt));
    }
    return out;
}

uint32_t basins_sizes(std::vector<basin> basins) {
    ranges::sort(basins, {}, [](const basin& b) { return -b.size(); });

    uint32_t sz = 1;
    for (auto& b : basins | views::take(3))
        sz *= b.size();
    return sz;
}
