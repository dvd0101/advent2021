#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <optional>
#include <variant>
#include <vector>

namespace smath {
    template <class... Ts>
    struct overloaded : Ts... {
        using Ts::operator()...;
    };

    template <class... Ts>
    overloaded(Ts...) -> overloaded<Ts...>;

    struct tree {
        using pointer = std::unique_ptr<tree>;

        tree* parent = nullptr;

        std::variant<pointer, int32_t> left;
        std::variant<pointer, int32_t> right;

        tree* left_child() {
            if (auto ptr = std::get_if<pointer>(&left); ptr) {
                return ptr->get();
            }

            return nullptr;
        }

        tree* right_child() {
            if (auto ptr = std::get_if<pointer>(&right); ptr)
                return ptr->get();
            return nullptr;
        }

        int32_t* left_value() {
            if (auto ptr = std::get_if<int32_t>(&left); ptr)
                return ptr;
            return nullptr;
        }

        int32_t* rigth_value() {
            if (auto ptr = std::get_if<int32_t>(&right); ptr)
                return ptr;
            return nullptr;
        }

        bool has_left() const {
            auto ptr = std::get_if<pointer>(&left);
            return ptr == nullptr || *ptr;
        }

        bool has_right() const {
            auto ptr = std::get_if<pointer>(&right);
            return ptr == nullptr || *ptr;
        }
    };

    std::ostream& operator<<(std::ostream& os, const tree& t) {
        os << "[";
        auto match = overloaded{[&](int32_t v) { os << v; },
                                [&](const std::unique_ptr<tree>& p) {
                                    if (!p) {
                                        os << "null";
                                    } else {
                                        os << *p;
                                    }
                                }};
        std::visit(match, t.left);
        os << ",";
        std::visit(match, t.right);
        return os << "]";
    };

    std::unique_ptr<tree> parse_line(std::string in) {
        auto n = std::make_unique<tree>();
        std::vector<tree*> stack = {n.get()};

        bool parse_first = true;
        for (char c : in) {
            auto curr = stack.back();
            switch (c) {
                case '[': {
                    auto n = std::make_unique<tree>();
                    n->parent = curr;
                    stack.push_back(n.get());
                    if (parse_first) {
                        assert(!curr->has_left());
                        curr->left = std::move(n);
                    } else {
                        assert(!curr->has_right());
                        curr->right = std::move(n);
                    }
                    parse_first = true;
                    break;
                }
                case ']': stack.pop_back(); break;
                case ',':
                    assert(!curr->has_right());
                    parse_first = false;
                    break;
                default:
                    if (parse_first)
                        curr->left = c - '0';
                    else
                        curr->right = c - '0';
                    break;
            }
        }

        auto ptr = std::move(std::get<tree::pointer>(n->left));
        ptr->parent = nullptr;
        return ptr;
    }

    std::vector<std::unique_ptr<tree>> parse(std::istream& in) {
        std::vector<std::unique_ptr<tree>> out;
        std::string line;
        while (std::getline(in, line))
            out.push_back(parse_line(std::move(line)));
        return out;
    }

    tree* descend(tree& n, int depth) {
        if (depth == 0) {
            return &n;
        }
        if (auto ptr = n.left_child(); ptr) {
            if (auto r = descend(*ptr, depth - 1); r)
                return r;
        }
        if (auto ptr = n.right_child(); ptr) {
            if (auto r = descend(*ptr, depth - 1); r)
                return r;
        }
        return nullptr;
    }

    int32_t* prev_value(tree& n) {
        tree* parent = n.parent;
        tree* t = &n;
        while (parent && parent->left_child() == t) {
            t = parent;
            parent = parent->parent;
        }
        if (!parent)
            return nullptr;

        if (auto ptr = parent->left_value(); ptr)
            return ptr;

        t = parent->left_child();
        if (!t)
            return nullptr;

        while (t->right_child()) {
            t = t->right_child();
        }
        return t->rigth_value();
    }

    int32_t* next_value(tree& n) {
        tree* parent = n.parent;
        tree* t = &n;
        while (parent && parent->right_child() == t) {
            t = parent;
            parent = parent->parent;
        }
        if (!parent)
            return nullptr;

        if (auto ptr = parent->rigth_value(); ptr)
            return ptr;

        t = parent->right_child();
        if (!t)
            return nullptr;

        while (t->left_child()) {
            t = t->left_child();
        }
        return t->left_value();
    }

    tree* search_tree_to_split(tree& n) {
        if (auto ptr = n.left_value(); ptr && *ptr >= 10)
            return &n;
        else if (auto ptr = n.left_child(); ptr) {
            if (auto r = search_tree_to_split(*ptr); r)
                return r;
        }

        if (auto ptr = n.rigth_value(); ptr && *ptr >= 10)
            return &n;
        else if (auto ptr = n.right_child(); ptr) {
            if (auto r = search_tree_to_split(*ptr); r)
                return r;
        }
        return nullptr;
    }

    uint64_t magnitude(tree& n) {
        uint64_t a, b;
        if (auto ptr = n.left_value(); ptr)
            a = *ptr;
        else if (auto ptr = n.left_child(); ptr) {
            a = magnitude(*ptr);
        }

        if (auto ptr = n.rigth_value(); ptr)
            b = *ptr;
        else if (auto ptr = n.right_child(); ptr) {
            b = magnitude(*ptr);
        }

        return 3 * a + 2 * b;
    }

    void reduce(tree& n) {
        bool loop = true;
        while (loop) {
            loop = false;
            if (auto explode = descend(n, 4); explode) {
                if (auto ptr = prev_value(*explode); ptr) {
                    *ptr += *(explode->left_value());
                }
                if (auto ptr = next_value(*explode); ptr) {
                    *ptr += *(explode->rigth_value());
                }

                auto parent = explode->parent;
                if (parent->left_child() == explode) {
                    parent->left = 0;
                } else if (parent->right_child() == explode) {
                    parent->right = 0;
                }
                loop = true;
            } else if (auto split = search_tree_to_split(n); split) {
                auto n = std::make_unique<tree>();
                n->parent = split;

                if (auto ptr = split->left_value(); ptr && *ptr >= 10) {
                    auto dr = std::div(*ptr, 2);
                    n->left = dr.quot;
                    n->right = dr.quot + dr.rem;
                    split->left = std::move(n);
                } else {
                    auto dr = std::div(*split->rigth_value(), 2);
                    n->left = dr.quot;
                    n->right = dr.quot + dr.rem;
                    split->right = std::move(n);
                }
                loop = true;
            }
        }
    }

    std::unique_ptr<tree> sum(std::unique_ptr<tree> a, std::unique_ptr<tree> b) {
        auto n = std::make_unique<tree>();
        a->parent = n.get();
        b->parent = n.get();
        n->left = std::move(a);
        n->right = std::move(b);
        reduce(*n);
        return n;
    }

    std::unique_ptr<tree> clone(tree& t) {
        auto n = std::make_unique<tree>();
        if (auto ptr = t.left_value(); ptr)
            n->left = *ptr;
        else if (auto ptr = t.left_child(); ptr) {
            n->left = clone(*ptr);
            n->left_child()->parent = n.get();
        }
        if (auto ptr = t.rigth_value(); ptr)
            n->right = *ptr;
        else if (auto ptr = t.right_child(); ptr) {
            n->right = clone(*ptr);
            n->right_child()->parent = n.get();
        }
        return n;
    }
}

int main() {
    std::ifstream in("./inputs/day18");
    auto numbers = smath::parse(in);

    auto n = sum(smath::clone(*numbers[0]), smath::clone(*numbers[1]));

    for (size_t i = 2; i < numbers.size(); i++) {
        n = sum(clone(*n), clone(*numbers[i]));
    }

    std::cout << ">>1 " << magnitude(*n) << "\n";

    uint64_t max_m = 0;
    for (size_t i = 0; i < numbers.size(); i++) {
        for (size_t j = 0; j < numbers.size(); j++) {
            if (i == j)
                continue;

            auto s = sum(clone(*numbers[i]), clone(*numbers[j]));
            uint64_t m = magnitude(*s);
            max_m = std::max(max_m, m);
        }
    }

    std::cout << ">>2 " << max_m << "\n";
}
