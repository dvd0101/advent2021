#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

struct whitespaces : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |= space;
        v['-'] |= space;
        v['>'] |= space;
        return &v[0];
    }
    whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

using flock = std::array<uint64_t, 9>;

void advance(flock& a);

int main() {
    std::ifstream in("./inputs/input11");
    in.imbue(std::locale(in.getloc(), new whitespaces));

    flock fishs{};
    int ttl;
    while (in >> ttl)
        fishs[ttl]++;

    for (int i = 0; i < 80; i++)
        advance(fishs);

    uint64_t total = std::accumulate(fishs.begin(), fishs.end(), (uint64_t)0);
    std::cout << ">>1 " << total << "\n";

    for (int i = 80; i < 256; i++)
        advance(fishs);

    total = std::accumulate(fishs.begin(), fishs.end(), (uint64_t)0);
    std::cout << ">>2 " << total << "\n";
}

void advance(flock& a) {
    std::rotate(std::begin(a), std::begin(a) + 1, std::end(a));
    a[6] += a[8];
}
