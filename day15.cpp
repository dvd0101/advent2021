#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <queue>
#include <set>
#include <vector>

namespace ranges = std::ranges;

struct coord {
    int32_t x;
    int32_t y;
    auto operator<=>(const coord& rhs) const = default;
};

std::ostream& operator<<(std::ostream&, const coord&);

struct rect_graph {
    int32_t line_size = 0;
    std::vector<int8_t> points;

    int32_t height() const { return std::ceil((float)points.size() / line_size); }
    int32_t width() const { return line_size; }

    bool contains(coord p) const { return p.x >= 0 && p.x < width() && p.y >= 0 && p.y < height(); }

    int8_t lookup(int32_t x, int32_t y) const {
        if (x < 0 || x >= width() || y < 0 || y >= height())
            return 10;
        return points[y * width() + x];
    }
    int8_t lookup(coord p) const { return lookup(p.x, p.y); }

    coord pos_to_coord(size_t i) const {
        int32_t y = i / width();
        int32_t x = i - y * width();
        return {x, y};
    }

    size_t coord_to_pos(coord c) const { return c.y * width() + c.x; }
};

std::ostream& operator<<(std::ostream&, const rect_graph&);

rect_graph parse(std::istream&);

uint64_t dijkstra(const rect_graph&, coord, coord);

rect_graph expand_graph(rect_graph);

int main() {
    std::ifstream in("./inputs/day15");
    auto graph = parse(in);

    uint64_t risk = dijkstra(graph, {0, 0}, {graph.width() - 1, graph.height() - 1});
    std::cout << ">>1 " << risk << "\n";

    graph = expand_graph(graph);
    risk = dijkstra(graph, {0, 0}, {graph.width() - 1, graph.height() - 1});
    std::cout << ">>2 " << risk << "\n";
}

std::ostream& operator<<(std::ostream& os, const coord& c) { return os << "(" << c.x << "," << c.y << ")"; }

std::ostream& operator<<(std::ostream& os, const rect_graph& g) {
    for (int32_t y = 0; y < g.height(); y++) {
        for (int32_t x = 0; x < g.width(); x++) {
            os << (int)g.lookup(x, y);
        }
        os << "\n";
    }
    return os;
}

rect_graph parse(std::istream& in) {
    rect_graph h;
    char c;
    while (in >> c)
        h.points.push_back(c - '0');

    in.clear();
    in.seekg(0);
    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    h.line_size = static_cast<int32_t>(in.tellg()) - 1;

    return h;
}

struct traverse_node {
    coord p;
    int64_t w;

    auto operator<=>(const traverse_node& rhs) const { return w <=> rhs.w; }
};

uint64_t dijkstra(const rect_graph& g, coord start, coord stop) {
    std::vector<int64_t> dists;
    dists.reserve(g.points.size());

    const auto dist = [&](coord a, coord b) { return std::abs(g.lookup(b) + g.lookup(a)); };
    const auto inf = std::numeric_limits<int64_t>::max();

    std::set<coord> stack;
    for (int32_t y = 0; y < g.height(); y++) {
        for (int32_t x = 0; x < g.width(); x++) {
            stack.insert({x, y});
            if (start.x == x && start.y == y)
                dists.push_back(0);
            else
                dists.push_back(inf);
        }
    }

    std::map<coord, coord> path;

    while (!stack.empty()) {
        auto it = ranges::min_element(dists);
        const size_t curr_pos = std::distance(dists.begin(), it);
        const int64_t curr_w = *it;
        const coord curr = g.pos_to_coord(curr_pos);

        stack.erase(curr);

        if (curr == stop) {
            break;
        }

        auto adjacent = {
            coord{curr.x - 1, curr.y}, //
            coord{curr.x + 1, curr.y}, //
            coord{curr.x, curr.y - 1}, //
            coord{curr.x, curr.y + 1}, //
        };

        for (auto adj : adjacent) {
            if (!stack.contains(adj))
                continue;
            auto w = curr_w + dist(curr, adj);
            auto ix = g.coord_to_pos(adj);
            if (w < dists[ix]) {
                dists[ix] = w;
                path[adj] = curr;
            }
        }
        dists[curr_pos] = inf;
    }

    uint64_t r = 0;
    while (stop != start) {
        r += g.lookup(stop);
        stop = path[stop];
    }

    return r;
}

rect_graph expand_graph(rect_graph tpl) {
    rect_graph g;
    g.line_size = tpl.line_size * 5;
    g.points.reserve(tpl.points.size() * 25);

    auto lookup = [&](int i, int32_t x, int32_t y) {
        int8_t v = tpl.lookup(x, y);
        v += i;
        if (v > 9)
            v = v - 9;
        return v;
    };

    for (int j = 0; j < 5; j++) {
        for (int32_t y = 0; y < tpl.height(); y++) {
            for (int i = 0; i < 5; i++) {
                for (int32_t x = 0; x < tpl.width(); x++) {
                    g.points.push_back(lookup(i, x, y));
                }
            }
        }

        for (auto& v : tpl.points) {
            v++;
            if (v > 9)
                v = v - 9;
        }
    }

    return g;
}
