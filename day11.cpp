#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <ranges>
#include <set>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

struct point {
    int32_t x;
    int32_t y;
};

struct bio_map {
    int32_t line_size = 0;
    std::vector<uint8_t> points;

    int32_t height() const { return points.size() / line_size; }
    int32_t width() const { return line_size; }

    template <typename F>
    void adjacent_cells(point p, F fn) const;

    point pos_to_point(int32_t pos) const;

    int32_t point_to_pos(point p) const;
};

std::ostream& operator<<(std::ostream& os, const bio_map& m);
bio_map parse(std::istream&);
uint64_t simulate(bio_map&);

int main() {
    std::ifstream in("./inputs/day11");
    auto octopuses = parse(in);

    uint64_t flashes = 0;
    for (int i = 0; i < 100; i++) {
        flashes += simulate(octopuses);
    }
    std::cout << ">>1 " << flashes << "\n";

    uint64_t step = 100;
    while (true) {
        step++;
        if (simulate(octopuses) == octopuses.points.size())
            break;
    }
    std::cout << ">>2 " << step << "\n";
}

std::ostream& operator<<(std::ostream& os, const bio_map& m) {
    for (int32_t y = 0; y < m.height(); y++) {
        for (int32_t x = 0; x < m.width(); x++) {
            os << (char)(m.points[y * m.width() + x] + '0');
        }
        os << "\n";
    }
    return os;
}

bio_map parse(std::istream& in) {
    bio_map h;
    char c;
    while (in >> c)
        h.points.push_back(c - '0');

    in.clear();
    in.seekg(0);
    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    h.line_size = static_cast<int32_t>(in.tellg()) - 1;

    return h;
}

uint64_t simulate(bio_map& m) {
    for (auto& c : m.points)
        c++;

    std::set<int32_t> flashes;
    std::vector<int32_t> stack;
    for (size_t i = 0; i < m.points.size(); i++) {
        if (m.points[i] > 9)
            stack.push_back(i);
    }

    while (!stack.empty()) {
        int32_t ix = stack.back();
        stack.pop_back();
        if (flashes.contains(ix))
            continue;

        point pt = m.pos_to_point(ix);

        flashes.insert(ix);
        m.adjacent_cells(pt, [&](int32_t i) {
            if (++m.points[i] > 9)
                stack.push_back(i);
        });
    }

    for (auto i : flashes)
        m.points[i] = 0;

    return flashes.size();
}

template <typename F>
void bio_map::adjacent_cells(point p, F fn) const {
    int32_t i;

    i = point_to_pos({p.x - 1, p.y - 1});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x + 0, p.y - 1});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x + 1, p.y - 1});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x - 1, p.y});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x + 1, p.y});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x - 1, p.y + 1});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x + 0, p.y + 1});
    if (i > -1)
        fn(i);

    i = point_to_pos({p.x + 1, p.y + 1});
    if (i > -1)
        fn(i);
}

point bio_map::pos_to_point(int32_t pos) const {
    int32_t y = pos / width();
    int32_t x = pos - y * width();
    return {x, y};
}

int32_t bio_map::point_to_pos(point p) const {
    auto [x, y] = p;
    if (x < 0 || x >= width() || y < 0 || y >= height())
        return -1;
    return y * width() + x;
}
