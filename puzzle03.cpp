#include <fstream>
#include <iostream>

int main()
{
    std::ifstream in("./inputs/input3");
    int horz = 0;
    int vert = 0;

    int x;
    std::string op;
    while (in >> op >> x) {
        if (op == "forward")
            horz += x;
        else if (op == "down")
            vert += x;
        else if (op == "up")
            vert -= x;
    }

    std::cout << ">>1 " << horz * vert << "\n";
}
