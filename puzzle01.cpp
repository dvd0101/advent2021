#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

struct sliding_windows {
    std::vector<int> values;

    int push(int v)
    {
        values.push_back(v);
        if (values.size() < 4)
            return 0;

        int w1 = values[0] + values[1] + values[2];
        int w2 = values[1] + values[2] + values[3];
        int inc = w2 > w1;

        using namespace std;
        rotate(begin(values), begin(values) + 1, end(values));
        values.resize(3);
        return inc;
    }
};

int main()
{
    std::ifstream in("./inputs/input1");
    std::vector<int> measures;

    {
        int x;
        while (in >> x)
            measures.push_back(x);
    }

    int increments = 0;
    for (size_t i = 1; i < measures.size(); i++) {
        if (measures[i] > measures[i - 1])
            increments++;
    }

    std::cout << ">>1 " << increments << "\n";

    sliding_windows sw;
    increments = 0;
    for (int v : measures)
        increments += sw.push(v);

    std::cout << ">>2 " << increments << "\n";
}
