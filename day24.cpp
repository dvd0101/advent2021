#include <array>
#include <bitset>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <span>
#include <variant>
#include <vector>

namespace sub {
    struct reg {
        uint8_t i;
    };

    using operand = std::variant<int64_t, reg>;

    enum class opcode { inp, add, mul, div, mod, eql, dump };

    struct ins {
        opcode code;
        reg arg1;
        operand arg2;
    };

    struct alu;
    std::ostream& operator<<(std::ostream& os, const alu& a);

    struct alu {
        std::array<int64_t, 4> regs;

        std::span<char> memory;
        size_t rx;

        void reset(std::span<char> m) {
            memory = m;
            rx = 0;
            for (auto& r : regs)
                r = 0;
        }

        void execute(const std::vector<ins>& inss) {
            for (auto& i : inss) {
                execute(i);
            }
        }

        void execute(const ins& in) {
            switch (in.code) {
                case opcode::inp: r(in.arg1) = memory[rx++]; break;
                case opcode::add: r(in.arg1) += v(in.arg2); break;
                case opcode::mul: r(in.arg1) *= v(in.arg2); break;
                case opcode::div: r(in.arg1) /= v(in.arg2); break;
                case opcode::mod: r(in.arg1) %= v(in.arg2); break;
                case opcode::eql: r(in.arg1) = r(in.arg1) == v(in.arg2); break;
                case opcode::dump: std::cout << *this << "\n"; break;
            };
        }

        int64_t& r(reg r) { return regs[r.i]; }

        int64_t v(operand o) {
            return std::visit(
                [&](auto&& arg) -> int64_t {
                    using T = std::decay_t<decltype(arg)>;
                    if constexpr (std::is_same_v<T, reg>)
                        return r(arg);
                    else
                        return arg;
                },
                o);
        }
    };

    std::ostream& operator<<(std::ostream& os, const alu& a) {
        os << "w: " << a.regs[0] << " ";
        os << "x: " << a.regs[1] << " ";
        os << "y: " << a.regs[2] << " ";
        os << "z: " << a.regs[3] << " ";
        os << "rx: " << a.rx;
        return os;
    }

    std::vector<ins> parse(std::istream& in) {
        std::string code, a1, a2;
        std::vector<ins> result;

        auto read_reg = [&in, &a1]() {
            in >> a1;
            return reg{static_cast<uint8_t>(a1[0] - 'w')};
        };

        auto read_value = [&in, &a2]() -> operand {
            in >> a2;
            if (a2 == "w" || a2 == "x" || a2 == "y" || a2 == "z") {
                return reg{static_cast<uint8_t>(a2[0] - 'w')};
            }
            return std::atoi(a2.c_str());
        };

        while (in >> code) {
            ins i;
            int args = 2;
            if (code == "inp") {
                i.code = opcode::inp;
                args = 1;
            } else if (code == "add") {
                i.code = opcode::add;
            } else if (code == "mul") {
                i.code = opcode::mul;
            } else if (code == "div") {
                i.code = opcode::div;
            } else if (code == "mod") {
                i.code = opcode::mod;
            } else if (code == "eql") {
                i.code = opcode::eql;
            } else if (code == "dump") {
                i.code = opcode::dump;
                args = 0;
            } else if (code[0] == '#') {
                std::getline(in, code);
                continue;
            } else {
                assert(0);
            }

            if (args >= 1)
                i.arg1 = read_reg();
            if (args >= 2)
                i.arg2 = read_value();

            result.push_back(i);
        }

        return result;
    }
}

int main() {
    // see inputs/day24
    std::cout << ">> 1 49917929934999\n";
    std::cout << ">> 2 11911316711816\n";
}
