#include <fstream>
#include <iostream>

int main()
{
    std::ifstream in("./inputs/input3");
    int horz = 0;
    int vert = 0;
    int aim = 0;

    int x;
    std::string op;
    while (in >> op >> x) {
        if (op == "forward") {
            horz += x;
            vert += aim * x;
        } else if (op == "down")
            aim += x;
        else if (op == "up")
            aim -= x;
    }

    std::cout << ">>2 " << horz * vert << "\n";
}
