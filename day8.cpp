#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <ranges>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::ranges::views;

struct note {
    std::array<std::string, 10> signals;
    std::array<std::string, 4> digits;
};

std::vector<note> parse(std::istream&);

// map a signal to the canonical name, eg:
// ['b', 'd', 'e', 'c', 'f', 'g', 'a']
//
// signal "a" (index 0) from the scrambled note is decoded to "b"
// signal "b" (index 1) from the scrambled note is decoded to "d"
using signal_map = std::array<char, 7>;
signal_map decode_signals(const std::array<std::string, 10>&);

using digits = std::array<uint8_t, 4>;
digits decode_digits(const std::array<std::string, 4>&, const signal_map&);

int decode_note(const note&);

int main() {
    std::ifstream in("./inputs/day8");

    auto notes = parse(in);

    int32_t simple_digits = 0;
    for (const auto& n : notes) {
        simple_digits += ranges::count_if(                                                             //
            n.digits,                                                                                  //
            [](auto& d) { return d.size() == 2 || d.size() == 4 || d.size() == 3 || d.size() == 7; }); //
    }
    std::cout << ">>1 " << simple_digits << "\n";

    int32_t result = 0;
    for (const auto& n : notes) {
        result += decode_note(n);
    }
    std::cout << ">>2 " << result << "\n";
}

std::vector<note> parse(std::istream& in) {
    std::vector<note> out;

    int flag = 0;
    int i = 0;
    std::string word;
    while (in >> word) {
        if (flag == 0) {
            out.push_back({});
            flag = 1;
            i = 0;
        }

        if (word == "|") {
            flag = 2;
            i = 0;
            continue;
        }

        ranges::sort(word);

        if (flag == 1) {
            out.back().signals[i++] = word;
        } else if (flag == 2) {
            out.back().digits[i++] = word;
            if (i == 4)
                flag = 0;
        }
    }

    return out;
}

/**
   aaaa
  b    c
  b    c
   dddd
  e    f
  e    f
   gggg
  */
signal_map decode_signals(const std::array<std::string, 10>& sigs) {
    signal_map out = {};

    std::string s1, s4, s7, s8;
    std::vector<std::string> s2_3_5;
    std::vector<std::string> s0_6_9;
    for (auto& s : sigs) {
        switch (s.size()) {
            case 2: s1 = s; break;
            case 3: s7 = s; break;
            case 4: s4 = s; break;
            case 7: s8 = s; break;
            case 5: s2_3_5.push_back(s); break;
            case 6: s0_6_9.push_back(s); break;
        }
    }

    std::string tmp1;
    ranges::set_difference(s7, s1, std::back_inserter(tmp1));
    out[tmp1[0] - 'a'] = 'a';

    std::string s3;
    for (size_t i = 0; i < s2_3_5.size(); i++) {
        if (ranges::includes(s2_3_5[i], s1)) {
            s3 = s2_3_5[i];
        }
    }

    std::string e;
    for (size_t i = 0; i < s2_3_5.size(); i++) {
        if (s2_3_5[i] == s3)
            continue;
        // s2_3_5[i] can be 2 or 5
        std::string b_e;
        ranges::set_difference(s2_3_5[i], s3, std::back_inserter(b_e));
        if (ranges::includes(s4, b_e)) {
            // s2_3_5[i] == 5
            out[b_e[0] - 'a'] = 'b';
        } else {
            // s2_3_5[i] == 2
            e = b_e[0];
            out[b_e[0] - 'a'] = 'e';
        }
    }

    tmp1 = "";
    std::string tmp2;
    ranges::set_difference(s3, s7, std::back_inserter(tmp1));
    ranges::set_difference(tmp1, s4, std::back_inserter(tmp2));
    out[tmp2[0] - 'a'] = 'g';

    for (size_t i = 0; i < s0_6_9.size(); i++) {
        tmp1 = "";
        ranges::set_difference(s1, s0_6_9[i], std::back_inserter(tmp1));
        if (tmp1.size() == 1) {
            // s0_6_9[i] == 6
            out[tmp1[0] - 'a'] = 'c';
            out[(tmp1[0] == s1[0] ? s1[1] : s1[0]) - 'a'] = 'f';
        } else {
            // s0_6_9[i] is 0 or 9
            if (ranges::includes(s0_6_9[i], e)) {
                // s0_6_9[i] == 0
                tmp2 = "";
                ranges::set_difference(s8, s0_6_9[i], std::back_inserter(tmp2));
                out[tmp2[0] - 'a'] = 'd';
            }
        }
    }
    return out;
}

const std::map<std::string, uint8_t> canonical{
    {"abcefg", 0},
    {"cf", 1},
    {"acdeg", 2},
    {"acdfg", 3},
    {"bcdf", 4},
    {"abdfg", 5},
    {"abdefg", 6},
    {"acf", 7},
    {"abcdefg", 8},
    {"abcdfg", 9},
};

digits decode_digits(const std::array<std::string, 4>& d, const signal_map& sm) {
    digits out;

    auto t = [&](char c) { return sm[c - 'a']; };

    for (size_t i = 0; i < d.size(); i++) {
        std::string decoded;
        ranges::copy(d[i] | views::transform(t), std::back_inserter(decoded));
        ranges::sort(decoded);
        out[i] = canonical.at(decoded);
    }

    return out;
}

int decode_note(const note& n) {
    auto smap = decode_signals(n.signals);
    auto digits = decode_digits(n.digits, smap);
    return digits[0] * 1000 + digits[1] * 100 + digits[2] * 10 + digits[3];
}
