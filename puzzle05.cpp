#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

using measures = std::vector<std::bitset<12>>;

std::pair<uint64_t, uint64_t> power_consumption(const measures&);
std::pair<uint64_t, uint64_t> life_support(measures values);

int main()
{
    std::ifstream in("./inputs/input5");
    measures values;

    std::string x;
    while (in >> x) {
        assert(x.size() == 12);

        std::bitset<12> v;
        for (size_t i = 0; i < x.size(); i++)
            if (x[i] == '1')
                v.set(11 - i);
        values.push_back(v);
    }

    auto [gamma, epsilon] = power_consumption(values);
    std::cout << ">>1 " << gamma * epsilon << "\n";

    auto [oxygen, co2] = life_support(std::move(values));
    std::cout << ">>2 " << oxygen * co2 << "\n";
}

std::pair<uint64_t, uint64_t> power_consumption(const measures& values)
{
    std::array<int, 12> bits = {};
    for (const auto& v : values) {
        for (size_t i = 0; i < v.size(); i++) {
            if (v[i])
                bits[i]++;
        }
    }

    const int threshold = values.size() / 2;
    uint64_t gamma = 0;
    uint64_t epsilon = 0;
    for (size_t i = 0; i < bits.size(); i++) {
        if (bits[i] == 0)
            continue;
        int most_common = bits[i] > threshold;
        if (most_common) {
            gamma |= (uint64_t)1 << i;
        } else {
            epsilon |= (uint64_t)1 << i;
        }
    }

    return { gamma, epsilon };
}

std::pair<uint64_t, uint64_t> life_support(measures values)
{
    auto most_common_bit = [&](auto start, auto end, int position) -> int {
        const auto r = std::div(static_cast<unsigned long>(std::distance(start, end)), 2);
        const int threshold = r.quot + r.rem;

        int ones = 0;
        for (; start != end; start++)
            if (start->test(position))
                ones++;

        return ones >= threshold;
    };

    auto partition = [&](auto start, auto end, int bit) {
        const int mcb = most_common_bit(start, end, bit);
        return std::partition(start, end, [&](const auto& v) { return v[bit] == mcb; });
    };

    auto start = values.begin();
    auto end = values.end();
    auto middle = partition(start, end, 11);

    uint64_t oxygen;
    {
        auto e = middle;
        int bit = 10;
        while (start != std::prev(e))
            e = partition(start, e, bit--);
        oxygen = start->to_ulong();
    }

    uint64_t co2;
    {
        auto s = middle;
        int bit = 10;
        while (s != std::prev(end))
            s = partition(s, end, bit--);
        co2 = s->to_ulong();
    }

    return { oxygen, co2 };
}
