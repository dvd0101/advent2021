#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <ranges>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

struct whitespaces : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |= space;
        v['-'] |= space;
        v['>'] |= space;
        return &v[0];
    }
    whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

struct point {
    int x = 0;
    int y = 0;
    auto operator<=>(const point&) const = default;
};

std::ostream& operator<<(std::ostream& os, point p) { return os << "(" << p.x << "," << p.y << ")"; }

struct line {
    point a;
    point b;
};

struct sparse_canvas {
    std::map<point, int> points;
};

bool is_hv(const line& l);
void draw(sparse_canvas& canvas, line l);

int main() {
    std::ifstream in("./inputs/input9");
    in.imbue(std::locale(in.getloc(), new whitespaces));

    std::vector<line> lines;
    line l;
    while (in >> l.a.x >> l.a.y >> l.b.x >> l.b.y)
        lines.push_back(l);

    sparse_canvas c;
    for (auto& l : lines | views::filter(is_hv))
        draw(c, l);

    size_t overlap1 = ranges::count_if(c.points | views::values, [](int c) { return c > 1; });
    std::cout << ">>1 " << overlap1 << "\n";

    c.points.clear();
    for (auto& l : lines)
        draw(c, l);

    size_t overlap2 = ranges::count_if(c.points | views::values, [](int c) { return c > 1; });
    std::cout << ">>2 " << overlap2 << "\n";
}

bool is_hv(const line& l) { return (l.a.x == l.b.x) || (l.a.y == l.b.y); }

void draw(sparse_canvas& canvas, line l) {
    while (true) {
        canvas.points[l.a]++;

        if (l.a == l.b)
            break;

        if (l.a.x < l.b.x)
            l.a.x++;
        else if (l.a.x > l.b.x)
            l.a.x--;

        if (l.a.y < l.b.y)
            l.a.y++;
        else if (l.a.y > l.b.y)
            l.a.y--;
    }
}
