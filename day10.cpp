#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <ranges>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

struct syntax_line {
    bool corrupted = false;
    std::vector<char> stack;

    bool add(char c);
    bool complete() const { return stack.empty(); }
};

std::vector<syntax_line> parse(std::istream&);

uint32_t error_score(const std::vector<syntax_line>&);

std::pair<std::vector<char>, uint64_t> complete(const syntax_line&);

uint32_t autocomplete_score(const std::vector<syntax_line>&);

int main() {
    std::ifstream in("./inputs/day10");
    auto lines = parse(in);

    std::cout << ">>1 " << error_score(lines) << "\n";
    std::cout << ">>2 " << autocomplete_score(lines) << "\n";
}

bool syntax_line::add(char c) {
    bool open = c == '(' || c == '[' || c == '{' || c == '<';
    if (open) {
        stack.push_back(c);
        return true;
    }

    if (stack.empty()) {
        stack.push_back(c);
        corrupted = true;
        return false;
    }

    switch (stack.back()) {
        case '(': corrupted = c != ')'; break;
        case '[': corrupted = c != ']'; break;
        case '{': corrupted = c != '}'; break;
        case '<': corrupted = c != '>'; break;
    }

    if (!corrupted) {
        stack.pop_back();
    } else {
        stack.push_back(c);
    }

    return !corrupted;
}

std::vector<syntax_line> parse(std::istream& in) {
    std::vector<syntax_line> out;

    std::string s;
    while (std::getline(in, s)) {
        syntax_line line;
        for (auto ch : s) {
            if (!line.add(ch))
                break;
        }
        out.push_back(std::move(line));
    }

    return out;
}

uint32_t error_score(const std::vector<syntax_line>& lines) {
    uint32_t score = 0;

    for (auto& l : lines | views::filter([](auto& x) { return x.corrupted; })) {
        switch (l.stack.back()) {
            case ')': score += 3; break;
            case ']': score += 57; break;
            case '}': score += 1197; break;
            case '>': score += 25137; break;
        }
    }

    return score;
}

std::pair<std::vector<char>, uint64_t> complete(const syntax_line& l) {
    std::vector<char> out;
    uint64_t score = 0;

    auto stack = l.stack;
    while (!stack.empty()) {
        score *= 5;
        switch (stack.back()) {
            case '(':
                out.push_back(')');
                score += 1;
                break;
            case '[':
                out.push_back(']');
                score += 2;
                break;
            case '{':
                out.push_back('}');
                score += 3;
                break;
            case '<':
                out.push_back('>');
                score += 4;
                break;
        }
        stack.pop_back();
    }
    return {out, score};
}

uint32_t autocomplete_score(const std::vector<syntax_line>& lines) {
    std::vector<uint64_t> scores;
    for (auto& l : lines | views::filter([](auto& x) { return !x.corrupted && !x.complete(); })) {
        uint64_t score = std::get<1>(complete(l));
        scores.push_back(score);
    }

    ranges::sort(scores);
    return scores[scores.size() / 2];
}
