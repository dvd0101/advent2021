#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <ranges>
#include <set>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

namespace sensors {
    struct point {
        int32_t x = 0;
        int32_t y = 0;
        int32_t z = 0;

        auto operator<=>(const point&) const = default;

        point operator+(const point& b) const { return {x + b.x, y + b.y, z + b.z}; }
        point operator-(const point& b) const { return {x - b.x, y - b.y, z - b.z}; }
    };

    std::ostream& operator<<(std::ostream& os, const point& p) {
        return os << "(" << p.x << "," << p.y << "," << p.z << ")";
    }

    int64_t manhattan(const point& a, const point& b) {
        return std::abs(a.x - b.x) + std::abs(a.y - b.y) + std::abs(a.z - b.z);
    }

    struct X {
        std::array<int, 3> x;
        std::array<int, 3> y;
        std::array<int, 3> z;
    };

    std::ostream& operator<<(std::ostream& os, const X& t) {
        os << "x: " << t.x[0] << " " << t.x[1] << " " << t.x[2] << "\n";
        os << "y: " << t.y[0] << " " << t.y[1] << " " << t.y[2] << "\n";
        os << "z: " << t.z[0] << " " << t.z[1] << " " << t.z[2] << "\n";
        return os;
    }

    point operator*(const point& b, const X& t) {
        return {
            b.x * t.x[0] + b.y * t.x[1] + b.z * t.x[2],
            b.x * t.y[0] + b.y * t.y[1] + b.z * t.y[2],
            b.x * t.z[0] + b.y * t.z[1] + b.z * t.z[2],
        };
    }

    struct probe {
        point pt;

        auto operator<=>(const probe&) const = default;
        bool operator==(const point& p) const { return pt == p; };
    };

    std::ostream& operator<<(std::ostream& os, const probe& p) { return os << "P" << p.pt; }

    using probe_pack = std::array<probe, 12>;

    std::ostream& operator<<(std::ostream& os, const probe_pack& p) {
        os << "[";
        for (auto& x : p)
            os << x;
        return os << "]";
    }

    struct scanner {
        std::vector<probe> probes;

        bool contains(const point& pt) const {
            return ranges::find_if(probes, [&](auto& p) { return p.pt == pt; }) != probes.end();
        }
    };

    struct whitespaces : std::ctype<char> {
        static const mask* make_table() {
            static std::vector<mask> v(classic_table(), classic_table() + table_size);
            v[','] |= space;
            return &v[0];
        }
        whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
    };

    std::vector<scanner> parse(std::istream& in) {
        std::vector<scanner> result;

        in.imbue(std::locale(in.getloc(), new whitespaces));
        std::string line;
        while (in >> line >> line >> line >> line) {
            scanner s;
            point p;
            while (in >> p.x >> p.y >> p.z)
                s.probes.push_back({p});
            result.push_back(std::move(s));

            in.clear();
        }

        return result;
    }

    constexpr std::array<X, 24> rots = []() -> std::array<X, 24> {
        // clang-format off
        return {
            X{{ 1,  0,  0}, { 0,  1,  0}, { 0,  0,  1}},
            X{{ 1,  0,  0}, { 0,  0, -1}, { 0,  1,  0}},
            X{{ 1,  0,  0}, { 0, -1,  0}, { 0,  0, -1}},
            X{{ 1,  0,  0}, { 0,  0,  1}, { 0, -1,  0}},

            X{{-1,  0,  0}, { 0, -1,  0}, { 0,  0,  1}},
            X{{-1,  0,  0}, { 0,  0,  1}, { 0,  1,  0}},
            X{{-1,  0,  0}, { 0,  1,  0}, { 0,  0, -1}},
            X{{-1,  0,  0}, { 0,  0, -1}, { 0, -1,  0}},

            X{{ 0,  1,  0}, { 0,  0,  1}, { 1,  0,  0}},
            X{{ 0,  1,  0}, {-1,  0,  0}, { 0,  0,  1}},
            X{{ 0,  1,  0}, { 0,  0, -1}, {-1,  0,  0}},
            X{{ 0,  1,  0}, { 1,  0,  0}, { 0,  0, -1}},

            X{{ 0, -1,  0}, { 0,  0, -1}, { 1,  0,  0}},
            X{{ 0, -1,  0}, { 1,  0,  0}, { 0,  0,  1}},
            X{{ 0, -1,  0}, { 0,  0,  1}, {-1,  0,  0}},
            X{{ 0, -1,  0}, {-1,  0,  0}, { 0,  0, -1}},

            X{{ 0,  0,  1}, { 1,  0,  0}, { 0,  1,  0}},
            X{{ 0,  0,  1}, { 0, -1,  0}, { 1,  0,  0}},
            X{{ 0,  0,  1}, {-1,  0,  0}, { 0, -1,  0}},
            X{{ 0,  0,  1}, { 0,  1,  0}, {-1,  0,  0}},

            X{{ 0,  0, -1}, {-1,  0,  0}, { 0,  1,  0}},
            X{{ 0,  0, -1}, { 0,  1,  0}, { 1,  0,  0}},
            X{{ 0,  0, -1}, { 1,  0,  0}, { 0, -1,  0}},
            X{{ 0,  0, -1}, { 0, -1,  0}, {-1,  0,  0}},
        };
        // clang-format on
    }();

    struct SX {
        point origin;
        point offset;
        X rot;

        point operator*(const point& b) const { return (b * rot) - offset; }
    };

    std::optional<SX> scanner_trans(const scanner& sa, const scanner& sb) {
        auto find_common_probes = [&](point delta, const X& dir) {
            int counter = 0;
            for (auto& pa : sa.probes) {
                for (auto& pb : sb.probes) {
                    if (pa.pt == (pb.pt * dir) - delta) {
                        counter++;
                        break;
                    }
                }
            }
            return counter == 12;
        };

        for (auto& pa : sa.probes) {
            for (auto& pb : sb.probes) {
                for (auto& rot : rots) {
                    point delta = (pb.pt * rot) - pa.pt;
                    if (find_common_probes(delta, rot)) {
                        return SX{pa.pt - (pb.pt * rot), delta, rot};
                    }
                }
            }
        }

        return {};
    }

    void merge_scanners(scanner& a, const scanner& b, SX t) {
        for (auto& p : b.probes) {
            auto pt = t * p.pt;
            if (!a.contains(pt)) {
                a.probes.push_back({pt});
            }
        }
    };

    template <typename Fn>
    void comb(uint32_t n, uint32_t k, Fn func) {
        std::string bitmask(k, 1);
        bitmask.resize(n, 0);

        std::vector<size_t> indicies;
        indicies.resize(k);

        do {
            size_t j = 0;
            for (size_t i = 0; i < n; i++) {
                if (bitmask[i])
                    indicies[j++] = i;
            }
            func(indicies);
        } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    }

    struct scanners_map {
      public:
        scanners_map(std::vector<scanner> scanners);

        std::vector<scanner> scanners;
        std::map<std::pair<int, int>, SX> rels;
        std::vector<std::pair<int, int>> deps;

      private:
        std::map<std::pair<int, int>, SX> build_rels() const;
        std::vector<std::pair<int, int>> tsort() const;
    };

    scanners_map::scanners_map(std::vector<scanner> s) : scanners{std::move(s)} {
        rels = build_rels();
        deps = tsort();
    }

    std::map<std::pair<int, int>, SX> scanners_map::build_rels() const {
        std::map<std::pair<int, int>, SX> r;
        for (size_t i = 0; i < scanners.size(); i++) {
            for (size_t j = 0; j < scanners.size(); j++) {
                if (i == j)
                    continue;

                std::cout << "map " << i << " " << j << "\n";

                auto t = scanner_trans(scanners[i], scanners[j]);
                if (t)
                    r.insert({{i, j}, *t});
            }
        }
        return r;
    }

    std::vector<std::pair<int, int>> scanners_map::tsort() const {
        std::vector<std::pair<int, int>> sorted;
        std::vector<int> stack = {0};
        int counter;
        while (!stack.empty()) {
            int start = stack.back();
            stack.pop_back();

            for (auto& el : rels) {
                /* if (!el.second) */
                /*     continue; */
                auto key = el.first;
                if (key.first == start || key.second == start) {
                    auto key2 = std::pair(key.second, key.first);
                    auto it = ranges::find_if(sorted, [&](const auto& s) { return s == key || s == key2; });
                    if (it == sorted.end()) {
                        if (key.first == start) {
                            sorted.push_back(key);
                            stack.push_back(key.second);
                        } else {
                            sorted.push_back(key2);
                            stack.push_back(key.first);
                        }
                        counter++;
                    }
                }
            }
        }
        return sorted;
    }

    scanner combine_scanners(const scanners_map& smap) {
        auto sout = smap.scanners;

        for (auto key : smap.deps | views::reverse) {
            auto [dst, src] = key;
            merge_scanners(sout[dst], sout[src], smap.rels.at(key));
        }

        return sout[0];
    }

    int64_t max_distance(const scanners_map& smap) {
        std::function<point(int, point)> calc_origin = [&](int index, point offset) -> point {
            if (index == 0)
                return {};

            std::pair<int, int> start;
            for (auto key : smap.deps | views::reverse) {
                if (key.second == index) {
                    start = key;
                    break;
                }
            }

            auto m = smap.rels.at(start);
            point origin;
            if (offset == point{}) {
                origin = m.origin;
            } else {
                origin = (offset * m.rot) - m.offset;
            }

            if (start.first == 0) {
                return origin;
            } else {
                return calc_origin(start.first, origin);
            }
        };

        const size_t n = smap.scanners.size();

        std::vector<point> origins;
        origins.resize(n);
        for (size_t i = 0; i < n; i++) {
            origins[i] = calc_origin(i, {});
        }

        int64_t d = 0;
        comb(n, 2, [&](const std::vector<size_t>& indicies) {
            auto i = indicies[0];
            auto j = indicies[1];
            d = std::max(d, manhattan(origins[i], origins[j]));
        });

        return d;
    }
}

int main() {
    std::ifstream in("./inputs/day19");
    auto scanners = sensors::parse(in);

    auto smap = sensors::scanners_map(std::move(scanners));

    sensors::scanner scanner = sensors::combine_scanners(smap);
    std::cout << ">>1 " << scanner.probes.size() << "\n";
    std::cout << ">>2 " << sensors::max_distance(smap) << "\n";
}
