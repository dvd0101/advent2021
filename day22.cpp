#include <algorithm>
#include <cassert>
#include <chrono>
#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <regex>
#include <vector>

namespace ranges = std::ranges;

namespace reactor {
    struct cuboid {
        bool value;
        int32_t x0, x1;
        int32_t y0, y1;
        int32_t z0, z1;

        bool is_valid() const { return x0 <= x1 && y0 <= y1 && z0 <= z1; }

        int64_t volume() const {
            int64_t x = x1 - x0 + 1;
            int64_t y = y1 - y0 + 1;
            int64_t z = z1 - z0 + 1;
            return x * y * z;
        }
    };

    std::ostream& operator<<(std::ostream& os, const cuboid& c) {
        os << (c.value ? "on" : "off");
        os << " x=" << c.x0 << ".." << c.x1;
        os << ",y=" << c.y0 << ".." << c.y1;
        os << ",z=" << c.z0 << ".." << c.z1;
        return os;
    }

    std::optional<cuboid> intersect(const cuboid& a, const cuboid& b) {
        cuboid c;
        c.value = a.value;
        c.x0 = std::max(a.x0, b.x0);
        c.x1 = std::min(a.x1, b.x1);
        c.y0 = std::max(a.y0, b.y0);
        c.y1 = std::min(a.y1, b.y1);
        c.z0 = std::max(a.z0, b.z0);
        c.z1 = std::min(a.z1, b.z1);

        if (!c.is_valid())
            return {};
        return c;
    }

    std::vector<cuboid> subtract(const cuboid& a, const cuboid& b) {
        std::vector<cuboid> r;

        if (a.x0 < b.x0) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = a.x0;
            pnl.x1 = b.x0 - 1;
            pnl.y0 = a.y0;
            pnl.y1 = a.y1;
            pnl.z0 = a.z0;
            pnl.z1 = a.z1;
            r.push_back(pnl);
        }

        if (a.x1 > b.x1) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = b.x1 + 1;
            pnl.x1 = a.x1;
            pnl.y0 = a.y0;
            pnl.y1 = a.y1;
            pnl.z0 = a.z0;
            pnl.z1 = a.z1;
            r.push_back(pnl);
        }

        if (a.y0 < b.y0) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = b.x0;
            pnl.x1 = b.x1;
            pnl.y0 = a.y0;
            pnl.y1 = b.y0 - 1;
            pnl.z0 = a.z0;
            pnl.z1 = a.z1;
            r.push_back(pnl);
        }

        if (a.y1 > b.y1) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = b.x0;
            pnl.x1 = b.x1;
            pnl.y0 = b.y1 + 1;
            pnl.y1 = a.y1;
            pnl.z0 = a.z0;
            pnl.z1 = a.z1;
            r.push_back(pnl);
        }

        if (a.z0 < b.z0) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = b.x0;
            pnl.x1 = b.x1;
            pnl.y0 = b.y0;
            pnl.y1 = b.y1;
            pnl.z0 = a.z0;
            pnl.z1 = b.z0 - 1;
            r.push_back(pnl);
        }

        if (a.z1 > b.z1) {
            cuboid pnl;
            pnl.value = a.value;
            pnl.x0 = b.x0;
            pnl.x1 = b.x1;
            pnl.y0 = b.y0;
            pnl.y1 = b.y1;
            pnl.z0 = b.z1 + 1;
            pnl.z1 = a.z1;
            r.push_back(pnl);
        }

        return r;
    }

    std::vector<cuboid> parse(std::istream& in) {
        static std::regex re(R"((on|off)+ x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+))");
        std::vector<cuboid> rules;

        std::string line;
        while (std::getline(in, line)) {
            std::smatch match;
            assert(std::regex_match(line, match, re));

            cuboid c;
            c.value = match[1].compare("on") == 0;
            c.x0 = std::atoi(match[2].str().c_str());
            c.x1 = std::atoi(match[3].str().c_str());
            c.y0 = std::atoi(match[4].str().c_str());
            c.y1 = std::atoi(match[5].str().c_str());
            c.z0 = std::atoi(match[6].str().c_str());
            c.z1 = std::atoi(match[7].str().c_str());

            rules.push_back(c);
        }
        return rules;
    }

    int64_t reboot1(const std::vector<cuboid>& cuboids, std::optional<cuboid> limit = std::nullopt) {
        std::vector<cuboid> seq;

        for (auto c : cuboids) {
            if (limit) {
                auto w = intersect(c, *limit);
                if (w) {
                    c = *w;
                } else {
                    continue;
                }
            }
            for (size_t i = 0, n = seq.size(); i < n; i++) {
                if (!seq[i].is_valid()) {
                    continue;
                }

                auto w = intersect(seq[i], c);
                if (!w)
                    continue;

                auto sub = subtract(seq[i], *w);
                if (sub.empty()) {
                    seq[i].x0 = seq[i].x1 + 1;
                } else {
                    seq[i] = sub[0];
                    seq.insert(seq.end(), sub.begin() + 1, sub.end());
                }
            }

            if (c.value)
                seq.push_back(c);
        }

        return std::accumulate(seq.begin(), seq.end(), int64_t(0), [](int64_t t, auto& c) { return t + c.volume(); });
    }

    int64_t reboot2(std::vector<cuboid> cuboids) {
        std::vector<cuboid> seq;
        for (const auto& c : cuboids) {
            std::vector<cuboid> ints;
            if (c.value)
                ints.push_back(c);

            for (const auto& prev : seq) {
                if (auto w = intersect(prev, c); w) {
                    w->value = !prev.value;
                    ints.push_back(*w);
                }
            }

            ranges::copy(ints, std::back_inserter(seq));
        }

        int64_t r = 0;
        for (auto& c : seq) {
            if (c.value)
                r += c.volume();
            else
                r -= c.volume();
        }

        return r;
    }
}

int main() {
    std::ifstream in("./inputs/day22");
    auto rules = reactor::parse(in);

    using namespace std::chrono;
    using clock = std::chrono::high_resolution_clock;

    reactor::cuboid limit{true, -50, 50, -50, 50, -50, 50};

    auto t0 = clock::now();
    std::cout << ">>1 " << reboot1(rules, limit) << " ";
    std::cout << duration_cast<milliseconds>(clock::now() - t0).count() << "ms\n";

    std::cout << ">>2 " << reboot1(rules) << " ";
    std::cout << duration_cast<milliseconds>(clock::now() - t0).count() << "ms\n";

    t0 = clock::now();
    std::cout << ">>2 " << reboot2(rules) << " ";
    std::cout << duration_cast<milliseconds>(clock::now() - t0).count() << "ms\n";
}
