#include <algorithm>
#include <bitset>
#include <cassert>
#include <fstream>
#include <iostream>
#include <tuple>
#include <vector>

#include "./generator.h"

namespace ranges = std::ranges;

namespace binimage {
    using enanche_algo = std::bitset<512>;

    struct image {
        image(std::vector<uint8_t> px, int32_t w) : pixels{std::move(px)} {
            assert(px.size() % w == 0);
            width = w;
            height = pixels.size() / w;
        }

        std::vector<uint8_t> pixels;
        int32_t width;
        int32_t height;

        uint8_t& pixel(int32_t x, int32_t y) { return pixels[y * width + x]; }
        const uint8_t& pixel(int32_t x, int32_t y) const { return pixels[y * width + x]; }
    };

    std::ostream& operator<<(std::ostream& os, const image& img) {
        for (int32_t y = 0; y < img.height; y++) {
            for (int32_t x = 0; x < img.width; x++) {
                os << (img.pixel(x, y) ? '#' : '.');
            }
            os << "\n";
        }
        return os;
    }

    image enlarge(const image& i, uint8_t n) {
        std::vector<uint8_t> pixels;
        int32_t w = i.width + 2 * n;
        int32_t h = i.height + 2 * n;
        pixels.resize(w * h);

        image r(std::move(pixels), w);
        for (int32_t y = n; y < h - n; y++) {
            for (int32_t x = n; x < w - n; x++) {
                r.pixel(x, y) = i.pixel(x - n, y - n);
            }
        }

        return r;
    }

    struct slide_window {
        std::bitset<9> pixels_value;
        int32_t x;
        int32_t y;
    };

    Generator<slide_window> sliding_window(image img) {
        slide_window w;
        auto& px = w.pixels_value;

        auto read = [&](int32_t x, int32_t y) {
            px.reset();
            px.set(8, img.pixel(x - 1, y - 1));
            px.set(7, img.pixel(x + 0, y - 1));
            px.set(6, img.pixel(x + 1, y - 1));
            px.set(5, img.pixel(x - 1, y + 0));
            px.set(4, img.pixel(x + 0, y + 0));
            px.set(3, img.pixel(x + 1, y + 0));
            px.set(2, img.pixel(x - 1, y + 1));
            px.set(1, img.pixel(x + 0, y + 1));
            px.set(0, img.pixel(x + 1, y + 1));
        };

        for (int32_t y = 0; y < img.height; y++) {
            for (int32_t x = 0; x < img.width; x++) {
                w.x = x;
                w.y = y;

                int32_t rx = std::clamp(x, 1, img.width - 2);
                int32_t ry = std::clamp(y, 1, img.height - 2);

                read(rx, ry);

                co_yield w;
            }
        }
    }

    std::tuple<image, enanche_algo> parse(std::ifstream& in) {
        std::string line;

        enanche_algo algo;
        std::vector<uint8_t> pixels;
        int32_t width = 0;

        bool parse_image = false;
        while (std::getline(in, line)) {
            if (!parse_image && line == "") {
                parse_image = true;
            } else if (!parse_image && line != "") {
                assert(line.size() == 512);
                for (size_t i = 0; i < line.size(); i++) {
                    if (line[i] == '#')
                        algo.set(i);
                }
            } else {
                if (width == 0)
                    width = line.size();
                else
                    assert(static_cast<size_t>(width) == line.size());

                for (auto ch : line)
                    pixels.push_back(ch == '#' ? 1 : 0);
            }
        }

        return {image(pixels, width), algo};
    }

    void enanche(image& img, const enanche_algo& algo) {
        auto g = sliding_window(img);
        while (g) {
            auto w = g();
            auto index = w.pixels_value.to_ulong();
            img.pixel(w.x, w.y) = algo.test(index);
        }
    }
}

int main() {
    std::ifstream in("./inputs/day20");

    auto [img, algo] = binimage::parse(in);
    img = enlarge(img, 52);

    for (int i = 0; i < 2; i++) {
        enanche(img, algo);
    }
    std::cout << ">>1 " << ranges::count(img.pixels, 1) << "\n";

    for (int i = 2; i < 50; i++) {
        enanche(img, algo);
    }
    std::cout << ">>2 " << ranges::count(img.pixels, 1) << "\n";
}
