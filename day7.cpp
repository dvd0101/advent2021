#include <climits>
#include <concepts>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

namespace ranges = std::ranges;

struct whitespaces : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |= space;
        return &v[0];
    }
    whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

// clang-format off
template <typename Fn>
concept EstimateFn = std::invocable<Fn, const std::vector<int32_t> &, int> && requires (Fn f) {
    { f({}, 0) } -> std::same_as<int32_t>;
};
// clang-format on

std::pair<int, int> find_best_alignment(const std::vector<int32_t>& p, EstimateFn auto& estimate_fn);
int32_t estimate_fuel(const std::vector<int32_t>& p, int dst);
int32_t estimate_fuel_non_linear(const std::vector<int32_t>& p, int dst);

int main() {
    std::ifstream in("./inputs/day7");
    in.imbue(std::locale(in.getloc(), new whitespaces));

    std::vector<int32_t> positions;
    std::copy(std::istream_iterator<int32_t>(in), std::istream_iterator<int32_t>(), std::back_inserter(positions));

    {
        auto [dst, fuel] = find_best_alignment(positions, estimate_fuel);
        std::cout << ">>1 " << fuel << " (p: " << dst << ")\n";
    }

    {
        auto [dst, fuel] = find_best_alignment(positions, estimate_fuel_non_linear);
        std::cout << ">>2 " << fuel << " (p: " << dst << ")\n";
    }
}

int32_t estimate_fuel(const std::vector<int32_t>& p, int dst) {
    int32_t v = 0;
    for (auto pos : p)
        v += std::abs(pos - dst);
    return v;
}

int32_t estimate_fuel_non_linear(const std::vector<int32_t>& p, int dst) {
    int32_t v = 0;
    for (auto pos : p) {
        int32_t x = std::abs(pos - dst);
        while (x > 0)
            v += x--;
    }
    return v;
}

std::pair<int, int> find_best_alignment(const std::vector<int32_t>& p, EstimateFn auto& estimate_fn) {
    const int32_t start = std::accumulate(p.begin(), p.end(), 0) / p.size();

    int32_t dst = start;
    int32_t fuel = estimate_fn(p, dst);

    auto explore = [&](auto change) {
        int32_t x = start;
        while (true) {
            x = change(x);
            auto f = estimate_fn(p, x);
            if (f > fuel) {
                break;
            }
            dst = x;
            fuel = f;
        }
    };

    explore([](int32_t x) { return x + 1; });
    explore([](int32_t x) { return x - 1; });

    return {dst, fuel};
}
