#include <cassert>
#include <fstream>
#include <iostream>
#include <limits>
#include <regex>
#include <set>

struct point {
    int32_t x = 0;
    int32_t y = 0;

    auto operator<=>(const point&) const = default;
};

std::ostream& operator<<(std::ostream& os, point p) { return os << "(" << p.x << "," << p.y << ")"; }

std::pair<point, point> parse(std::istream&);

std::pair<int32_t, int32_t> simulate(point, point);

int main() {
    std::ifstream in("./inputs/day17");

    auto [p0, p1] = parse(in);
    auto [max_y, vels] = simulate(p0, p1);
    std::cout << ">>1 " << max_y << "\n";
    std::cout << ">>2 " << vels << "\n";
}

std::pair<point, point> parse(std::istream& in) {
    std::regex re("x=(-?\\d+)\\.\\.(-?\\d+), y=(-?\\d+)\\.\\.(-?\\d+)");
    std::smatch sm;

    std::string line;
    std::getline(in, line);
    assert(std::regex_search(line, sm, re));

    return {
        point{std::stoi(sm[1].str()), std::stoi(sm[3].str())},
        point{std::stoi(sm[2].str()), std::stoi(sm[4].str())},
    };
}

std::pair<int32_t, int32_t> simulate(point p0, point p1) {
    auto inside = [&](point p) { return (p.x >= p0.x && p.x <= p1.x) && (p.y >= p0.y && p.y <= p1.y); };
    std::set<point> velocities;

    auto run = [&](int32_t vx, int32_t vy) {
        point pos;
        int32_t max_y = std::numeric_limits<int32_t>::min();
        bool hit = false;

        while (true) {
            pos.x += vx;
            pos.y += vy;
            max_y = std::max(max_y, pos.y);

            hit |= inside(pos);

            vx = std::max(0, vx - 1);
            vy--;

            if (vx == 0 && pos.x < p0.x)
                break;
            if (pos.x > p1.x)
                break;
            if (pos.y < p0.y)
                break;
        }

        return std::pair(hit, max_y);
    };

    int32_t best = 0;
    int32_t vy = p0.y;
    while (vy < 1000) {
        for (int32_t vx = 1; vx <= p1.x; vx++) {
            auto [hit, y] = run(vx, vy);
            if (hit) {
                velocities.insert({vx, vy});
                best = std::max(best, y);
            }
        }
        vy++;
    }

    return {best, velocities.size()};
}
