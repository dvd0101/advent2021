#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

namespace sub {
    struct point {
        int32_t x = 0;
        int32_t y = 0;

        auto operator<=>(const point&) const = default;
    };

    std::ostream& operator<<(std::ostream& os, point pt) { return os << "(" << pt.x << "," << pt.y << ")"; }

    struct grid {
        int32_t width = 0;
        int32_t height = 0;

        enum cell_type { to_east, to_south, empty };
        std::vector<cell_type> cells;

        cell_type lookup(int32_t x, int32_t y) const { return cells.at(y * width + x); }
        cell_type& lookup(int32_t x, int32_t y) { return cells.at(y * width + x); }
        cell_type& lookup(point pt) { return cells.at(pt.y * width + pt.x); }

        point to_point(size_t i) const {
            int32_t y = i / width;
            int32_t x = i - y * width;
            return {x, y};
        }
    };

    std::ostream& operator<<(std::ostream& os, const grid& g) {
        for (int32_t y = 0; y < g.height; y++) {
            for (int32_t x = 0; x < g.width; x++) {
                switch (g.lookup(x, y)) {
                    case grid::to_east: os << ">"; break;
                    case grid::to_south: os << "v"; break;
                    default: os << "."; break;
                }
            }
            os << "\n";
        }
        return os;
    }

    uint64_t simulate_until_stop(grid g) {
        uint64_t counter = 1;
        while (true) {

            std::map<point, point> record;

            for (size_t i = 0; i < g.cells.size(); i++) {
                if (g.cells[i] != grid::to_east)
                    continue;
                point src = g.to_point(i);
                point dst = src;
                dst.x = (dst.x + 1) % g.width;
                if (g.lookup(dst) == grid::empty) {
                    record.insert({src, dst});
                }
            }

            bool moved = !record.empty();
            for (auto [src, dst] : record) {
                g.lookup(src) = grid::empty;
                g.lookup(dst) = grid::to_east;
            }

            record.clear();

            for (size_t i = 0; i < g.cells.size(); i++) {
                if (g.cells[i] != grid::to_south)
                    continue;
                point src = g.to_point(i);
                point dst = src;
                dst.y = (dst.y + 1) % g.height;
                if (g.lookup(dst) == grid::empty) {
                    record.insert({src, dst});
                }
            }

            moved |= !record.empty();
            for (auto [src, dst] : record) {
                g.lookup(src) = grid::empty;
                g.lookup(dst) = grid::to_south;
            }

            if (!moved)
                break;
            counter++;
        }
        return counter;
    }

    grid parse(std::istream& in) {
        grid g;

        std::string line;
        while (std::getline(in, line)) {
            if (g.width == 0)
                g.width = line.size();
            else
                assert(g.width == (int32_t)line.size());

            for (size_t x = 0; x < line.size(); x++) {
                char ch = line[x];
                switch (ch) {
                    case '>': g.cells.push_back(grid::to_east); break;
                    case 'v': g.cells.push_back(grid::to_south); break;
                    case '.': g.cells.push_back(grid::empty); break;
                    default: assert(0);
                }
            }

            g.height++;
        }

        return g;
    }
}

int main() {
    std::ifstream in("./inputs/day25");
    auto grid = sub::parse(in);
    std::cout << grid << "\n";
    std::cout << ">>1 " << sub::simulate_until_stop(grid) << "\n";
}
