#include <array>
#include <iostream>
#include <unordered_map>
#include <vector>

namespace ddirac {
    struct player {
        int32_t pos = 0;
        int32_t score = 0;

        auto operator<=>(const player&) const = default;

        void advance(int32_t r) {
            pos = (pos + r - 1) % 10 + 1;
            score += pos;
        }
    };

    std::ostream& operator<<(std::ostream& os, player p) { return os << "(" << p.pos << " => " << p.score << ")"; }

    struct deterministic_dice {
        int32_t value = 0;

        int32_t roll() {
            auto a = (value++ % 100) + 1;
            auto b = (value++ % 100) + 1;
            auto c = (value++ % 100) + 1;
            return a + b + c;
        }
    };

    struct dirac_dice {
        constexpr static std::array<int, 27> roll() {
            std::array<int, 27> r;
            int pos = 0;
            for (int i = 1; i <= 3; i++)
                for (int j = 1; j <= 3; j++)
                    for (int z = 1; z <= 3; z++)
                        r[pos++] = i + j + z;
            return r;
        }
    };

    template <class T>
    inline void hash_combine(std::size_t& seed, const T& v) {
        std::hash<T> hasher;
        seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }

    struct game_state {
        player p1;
        player p2;

        auto operator<=>(const game_state&) const = default;
    };

}

namespace std {
    template <>
    struct hash<ddirac::game_state> {
        std::size_t operator()(ddirac::game_state const& s) const noexcept {
            size_t h1 = std::hash<int32_t>{}(s.p1.pos);
            ddirac::hash_combine(h1, std::hash<int32_t>{}(s.p1.score));
            ddirac::hash_combine(h1, std::hash<int32_t>{}(s.p2.pos));
            ddirac::hash_combine(h1, std::hash<int32_t>{}(s.p2.score));
            return h1;
        }
    };
}

namespace ddirac {
    uint64_t game1(int32_t pos1, int32_t pos2, int32_t limit) {
        player p1{pos1, 0};
        player p2{pos2, 0};
        deterministic_dice dice;

        int rolls = 0;
        while (true) {
            p1.advance(dice.roll());
            rolls += 3;
            if (p1.score >= limit)
                break;

            p2.advance(dice.roll());
            rolls += 3;
            if (p2.score >= limit)
                break;
        }

        int32_t loser = std::min(p1.score, p2.score);
        return loser * rolls;
    }

    uint64_t game2(int32_t pos1, int32_t pos2, int32_t limit) {
        auto rolls = dirac_dice::roll();

        uint64_t p1_win = 0;
        uint64_t p2_win = 0;

        using games_state = std::unordered_map<game_state, uint64_t>;
        games_state games;

        games.insert({{pos1, 0, pos2, 0}, 1});

        while (!games.empty()) {
            games_state new_state;

            for (auto [state, num] : games) {
                for (auto r1 : rolls) {
                    game_state s = state;

                    s.p1.advance(r1);
                    if (s.p1.score >= limit) {
                        p1_win += num;
                    } else {
                        for (auto r2 : rolls) {
                            auto s2 = s;

                            s2.p2.advance(r2);
                            if (s2.p2.score >= limit) {
                                p2_win += num;
                            } else {
                                new_state[s2] += num;
                            }
                        }
                    }
                }
            }

            games = std::move(new_state);
        }

        return std::max(p1_win, p2_win);
    }
}

int main() {
    uint64_t r = ddirac::game1(9, 10, 1000);
    std::cout << ">>1 " << r << "\n";

    r = ddirac::game2(9, 10, 21);
    std::cout << ">>2 " << r << "\n";
}
