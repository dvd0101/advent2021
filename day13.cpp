#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace ranges = std::ranges;

struct point {
    int x = 0;
    int y = 0;

    auto operator<=>(const point&) const = default;
};

struct rule {
    int fold_x = 0;
    int fold_y = 0;
};

using sheet = std::set<point>;
using rules = std::vector<rule>;

std::pair<sheet, rules> parse(std::istream&);
void apply_rule(sheet&, rule);
std::ostream& operator<<(std::ostream&, const sheet&);

int main() {
    std::ifstream in("./inputs/day13");
    auto [sheet, rules] = parse(in);

    apply_rule(sheet, rules[0]);
    std::cout << ">>1 " << sheet.size() << "\n";

    std::cout << ">>2\n";
    for (size_t i = 1; i < rules.size(); i++) {
        apply_rule(sheet, rules[i]);
    }

    std::cout << sheet << "\n";
}

struct whitespaces : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |= space;
        return &v[0];
    }
    whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

std::pair<sheet, rules> parse(std::istream& in) {
    sheet s;
    rules r;

    std::string line;
    std::istringstream ss;
    ss.imbue(std::locale(ss.getloc(), new whitespaces));

    point pt;
    bool in_sheet = true;
    while (std::getline(in, line)) {
        if (line == "") {
            in_sheet = false;
        } else if (in_sheet) {
            ss.str(line);
            ss.seekg(0);
            ss >> pt.x >> pt.y;
            s.insert(pt);
        } else {
            rule nr;
            ss.str(line);
            auto pos = line.find("x=");
            if (pos != std::string::npos) {
                ss.seekg(pos + 2);
                ss >> nr.fold_x;
            } else {
                pos = line.find("y=");
                assert(pos != std::string::npos);
                ss.seekg(pos + 2);
                ss >> nr.fold_y;
            }
            r.push_back(nr);
        }
    }

    return {s, r};
}

void apply_rule(sheet& s, rule r) {
    sheet out;

    if (r.fold_x > 0) {
        sheet s2 = s;
        for (auto pt : s2) {
            if (pt.x > r.fold_x) {
                s.erase(pt);
                pt.x = r.fold_x - (pt.x - r.fold_x);
            }
            s.insert(pt);
        }
    } else {
        sheet s2 = s;
        for (auto pt : s2) {
            if (pt.y > r.fold_y) {
                s.erase(pt);
                pt.y = r.fold_y - (pt.y - r.fold_y);
            }
            s.insert(pt);
        }
    }
}

std::ostream& operator<<(std::ostream& os, const sheet& s) {
    int width = ranges::max(s, {}, [](point p) { return p.x; }).x;
    int height = ranges::max(s, {}, [](point p) { return p.y; }).y;
    for (int y = 0; y <= height; y++) {
        for (int x = 0; x <= width; x++) {
            if (s.contains({x, y}))
                os << "#";
            else
                os << ".";
        }
        os << "\n";
    }
    return os;
}
