#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <numeric>
#include <ranges>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

struct bit_bucket {
    void push(uint8_t val, uint8_t bits) {
        assert(bits == 4 || bits == 1);
        if (bytes.empty() || last_bits == 8) {
            bytes.push_back(val << (8 - bits));
            last_bits = bits;
        } else {
            bytes.back() |= val << (8 - last_bits - bits);
            last_bits += bits;
        }
    }

    uint64_t pop(uint8_t bits) {
        uint64_t out = 0;
        /* int x = bits; */
        while (bits--) {
            out <<= 1;
            if (rindex == -1 || top_bits == 0) {
                rindex++;
                top_bits = 8;
            }
            uint8_t bit = bytes.at(rindex) & (1 << (top_bits - 1));
            out |= bit ? 1 : 0;
            top_bits--;
        }
        return out;
    }

    bool empty() const { return bits() == 0; }

    uint64_t bits() const {
        auto n = static_cast<int64_t>(bytes.size());
        if (rindex > 0) {
            n -= rindex;
        }
        return n * 8 - (8 - top_bits) - (8 - last_bits);
    }

    std::vector<uint8_t> bytes;
    int64_t rindex = -1;
    uint8_t top_bits = 8;
    uint8_t last_bits = 0;
};

enum class packet_type { sum = 0, product, minimum, maximum, literal, greater, lesser, equal };

struct packet {
    uint8_t version;
    packet_type type_id;
    std::vector<packet> sub_packets;
    uint64_t payload;
};

bit_bucket decode(std::istream&);
packet parse(bit_bucket&);
uint64_t collect_versions(const packet&);
uint64_t calculate(const packet&);

int main() {
    std::ifstream in("./inputs/day16");

    bit_bucket bucket = decode(in);
    packet pkt = parse(bucket);

    std::cout << ">>1 " << collect_versions(pkt) << "\n";
    std::cout << ">>2 " << calculate(pkt) << "\n";
}

bit_bucket decode(std::istream& in) {
    bit_bucket bucket;
    char c;
    while (in >> c) {
        uint8_t n;
        if (c >= 'A')
            n = 10 + c - 'A';
        else
            n = c - '0';
        bucket.push(n, 4);
    }
    return bucket;
}

uint64_t parse_literal(bit_bucket& b) {
    uint64_t out = 0;
    int i = 0;
    while (true) {
        assert(i++ < 16);
        uint8_t v = b.pop(5);
        out = (out << 4) | (v & 0xf);
        if ((v & 0x10) == 0)
            break;
    }
    return out;
}

packet parse(bit_bucket& bucket) {
    packet pkt;

    pkt.version = bucket.pop(3);
    pkt.type_id = static_cast<packet_type>(bucket.pop(3));

    if (pkt.type_id == packet_type::literal) {
        pkt.payload = parse_literal(bucket);
    } else {
        uint8_t length_type = bucket.pop(1);
        if (length_type == 0) {
            uint64_t bits = bucket.pop(15);
            bit_bucket b2;
            while (bits--) {
                b2.push(bucket.pop(1), 1);
            }
            while (!b2.empty()) {
                pkt.sub_packets.push_back(parse(b2));
            }
        } else {
            uint64_t packets = bucket.pop(11);
            while (packets--) {
                pkt.sub_packets.push_back(parse(bucket));
            }
        }
    }

    return pkt;
}

uint64_t collect_versions(const packet& pkt) {
    uint64_t version = pkt.version;
    for (auto& ch : pkt.sub_packets) {
        version += collect_versions(ch);
    }
    return version;
}

uint64_t calculate(const packet& pkt) {
    if (pkt.type_id == packet_type::literal)
        return pkt.payload;

    uint64_t r;
    auto values = pkt.sub_packets | views::transform([](auto& p) { return calculate(p); });
    switch (pkt.type_id) {
        case packet_type::sum: r = std::accumulate(values.begin(), values.end(), 0ull, std::plus<>()); break;
        case packet_type::product: r = std::accumulate(values.begin(), values.end(), 1ull, std::multiplies<>()); break;
        case packet_type::minimum: r = ranges::min(values); break;
        case packet_type::maximum: r = ranges::max(values); break;
        case packet_type::literal: r = pkt.payload; break;
        case packet_type::greater: r = values[0] > values[1] ? 1 : 0; break;
        case packet_type::lesser: r = values[0] < values[1] ? 1 : 0; break;
        case packet_type::equal: r = values[0] == values[1] ? 1 : 0; break;
    }

    return r;
}
