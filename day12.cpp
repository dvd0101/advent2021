#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

namespace ranges = std::ranges;

struct cave {
    std::string name;

    auto operator<=>(const cave&) const = default;

    bool is_big() const {
        for (auto c : name)
            if (std::islower(c))
                return false;
        return true;
    }
};

namespace std {
    template <>
    struct hash<cave> {
        std::size_t operator()(cave const& s) const noexcept { return std::hash<std::string>{}(s.name); }
    };
}

struct path {
    std::vector<std::string> elements;
    std::string allowed_twice;

    path(std::string n, std::string t) : elements{std::move(n)}, allowed_twice{std::move(t)} {}

    bool can_explore(const cave& c) const {
        if (c.is_big())
            return true;

        int limit = c.name == allowed_twice ? 2 : 1;
        return ranges::count(elements, c.name) < limit;
    }

    bool complete() const { return elements.back() == "end"; }
};

using nodes = std::unordered_map<cave, std::vector<cave>>;

nodes parse(std::istream&);

std::vector<path> available_paths(const nodes&);
std::vector<path> available_paths2(const nodes&);

int main() {
    std::ifstream in("./inputs/day12");
    auto graph = parse(in);

    std::cout << ">>1 " << available_paths(graph).size() << "\n";
    std::cout << ">>2 " << available_paths2(graph).size() << "\n";
}

struct whitespaces : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v['-'] |= space;
        return &v[0];
    }
    whitespaces(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

nodes parse(std::istream& in) {
    in.imbue(std::locale(in.getloc(), new whitespaces));

    nodes graph;

    cave a, b;
    while (in >> a.name >> b.name) {
        graph[a].push_back(b);
        if (a.name != "start")
            graph[b].push_back(a);
    }

    return graph;
}

template <typename FnValid, typename FnAdd>
std::vector<path> explore(const nodes& graph, FnValid is_valid, FnAdd add) {
    std::vector<path> paths;
    std::vector<path> stack = {{"start", ""}};

    while (!stack.empty()) {
        auto curr = std::move(stack.back());
        stack.pop_back();

        for (auto& next : graph.at({curr.elements.back()})) {
            if (!curr.can_explore(next))
                continue;

            auto new_path = curr;
            new_path.elements.push_back(next.name);

            if (new_path.complete()) {
                if (is_valid(new_path))
                    paths.push_back(std::move(new_path));
            } else {
                add(stack, std::move(new_path), next);
            }
        }
    }

    return paths;
}

std::vector<path> available_paths(const nodes& graph) {
    auto is_valid_path = [](const path&) { return true; };

    auto add_path = [](std::vector<path>& stack, path p, const cave&) { stack.push_back(std::move(p)); };

    return explore(graph, is_valid_path, add_path);
}

std::vector<path> available_paths2(const nodes& graph) {
    auto is_valid_path = [](const path& p) {
        return p.allowed_twice == "" || ranges::count(p.elements, p.allowed_twice) == 2;
    };

    auto add_path = [](std::vector<path>& stack, path p, const cave& last) {
        if (last.is_big() || p.allowed_twice != "") {
            stack.push_back(std::move(p));
        } else {
            stack.push_back(p);
            p.allowed_twice = last.name;
            stack.push_back(std::move(p));
        }
    };

    return explore(graph, is_valid_path, add_path);
}
