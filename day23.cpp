#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <optional>
#include <ranges>
#include <vector>

namespace ranges = std::ranges;
namespace views = std::views;

namespace amp {

    struct point {
        int32_t x = 0;
        int32_t y = 0;

        auto operator<=>(const point&) const = default;
        point operator+(point pt) const { return {x + pt.x, y + pt.y}; }
    };

    std::ostream& operator<<(std::ostream& os, point pt) { return os << "(" << pt.x << "," << pt.y << ")"; }

    struct board {
        board() {
            for (auto& t : tiles)
                t = wall;
        }

        constexpr static int width = 11;
        constexpr static int height = 5;

        std::array<std::vector<point>, 4> goals;

        enum tile_type { wall, soft_wall, hallway, room };
        std::array<tile_type, width * height> tiles;

        tile_type& tile(point p) { return tile(p.x, p.y); }
        tile_type& tile(int32_t x, int32_t y) {
            int32_t i = y * width + x;
            assert(i >= 0 && i < (int32_t)tiles.size());
            return tiles[i];
        }

        tile_type tile(point p) const { return tile(p.x, p.y); }
        tile_type tile(int32_t x, int32_t y) const {
            int32_t i = y * width + x;
            if (i < 0 || i >= (int32_t)tiles.size())
                return wall;
            return tiles[y * width + x];
        }

        point to_pos(size_t i) const {
            assert(i <= tiles.size());
            int32_t y = i / width;
            int32_t x = i % width;
            return {x, y};
        }
    };

    struct game;

    struct player {
        uint8_t id = 0;
        point pos;
        char type;
        int32_t cost = 0;
        int32_t energy = 0;

        bool win(const game& g) const;

        player move(point pt, int32_t steps) const {
            player b = *this;
            b.pos = pt;
            b.energy += steps * cost;
            return b;
        }
    };

    std::ostream& operator<<(std::ostream& os, const player& pl) {
        return os << "[" << pl.type << "(" << pl.pos.x << "," << pl.pos.y << "):" << pl.energy << "]";
    }

    struct game {
        board* _b = nullptr;
        std::vector<player> players;

        game() {
            for (uint8_t i = 0; i < players.size(); i++)
                players[i].id = i;
        }

        const board& b() const { return *_b; }
        board& b() { return *_b; }

        std::optional<player> at(point p) const { return at(p.x, p.y); }

        std::optional<player> at(int32_t x, int32_t y) const {
            point pos{x, y};
            for (auto& pl : players) {
                if (pl.pos == pos)
                    return pl;
            }
            return {};
        }

        auto tile(point p) const { return b().tile(p); }
        auto tile(int32_t x, int32_t y) const { return b().tile(x, y); }

        bool finished() const {
            for (auto& pl : players) {
                if (!pl.win(*this))
                    return false;
            }
            return true;
        }

        int32_t score() const {
            int32_t s = 0;
            for (auto& pl : players)
                s += pl.energy;
            return s;
        }
    };

    std::ostream& operator<<(std::ostream& os, const game& g) {
        for (int32_t y = 0; y < g.b().height; y++) {
            for (int32_t x = 0; x < g.b().width; x++) {
                if (auto pl = g.at(x, y); pl) {
                    os << pl->type;
                } else {
                    switch (g.b().tile(x, y)) {
                        case board::wall: os << "#"; break;
                        case board::soft_wall: os << "x"; break;
                        case board::hallway:
                        case board::room:
                        default: os << "."; break;
                    }
                }
            }
            os << "\n";
        }
        return os;
    }

    bool player::win(const game& g) const {
        bool same_type = true;
        for (auto pt : g.b().goals[type - 'A']) {
            if (pt == pos)
                break;

            auto pl = g.at(pt);
            if (!pl || pl->type != type) {
                same_type = false;
                break;
            }
        }
        return same_type;
    }

    int32_t dist(point a, point b) { return std::abs(a.x - b.x) + std::abs(a.y - b.y); }

    int32_t pathfind(const game& g, point src, point dst) {
        static std::array<point, 4> dirs = {0, 1, 1, 0, 0, -1, -1, 0};

        auto limit = dist(src, dst);
        int32_t steps = 0;

        while (src != dst) {
            /* std::cout << " " << src << " limit:" << limit << "\n"; */
            point k;
            for (auto dir : dirs) {
                point pt = src + dir;
                /* std::cout << "  --> " << pt << " " << dist(pt, dst) << "\n"; */
                if (g.tile(pt) == board::wall || dist(pt, dst) > limit)
                    continue;
                if (g.at(pt))
                    continue;
                k = dir;
            }

            if (k.x == 0 && k.y == 0)
                return -1;

            src = src + k;
            steps++;
            limit--;
        }
        return steps;
    }

    std::vector<player> possible_moves(const game& g, const player& pl, int32_t threshold) {
        static const std::vector<player> no_moves;

        // player already won, no moves necessary
        if (pl.win(g))
            return no_moves;

        static std::vector<point> targets;
        targets.clear();

        bool same_type = true;
        auto& goals = g.b().goals[pl.type - 'A'];
        for (auto pt : goals) {
            auto pl2 = g.at(pt);
            if (pl2 && pl2->type != pl.type) {
                same_type = false;
                break;
            }
        }
        /* std::cout << pl << " " << same_type << "\n"; */
        if (same_type) {
            bool f = false;
            for (size_t i = 0; i < goals.size(); i++) {
                auto curr = g.at(goals[i]);
                if (!curr) {
                    targets.push_back(goals[i]);
                    f = true;
                    // XXX
                    break;
                } else {
                    if (f)
                        break;
                }
            }
        }

        static std::vector<player> moves;
        moves.clear();
        for (auto dst : targets) {
            auto steps = pathfind(g, pl.pos, dst);
            if (steps > 0) {
                moves.push_back(pl.move(dst, steps));
            }
        }
        if (!moves.empty()) {
            assert(moves.size() == 1);
            return moves;
        }

        if (g.tile(pl.pos) == board::room) {
            // target one of the hallway tile

            auto& b = g.b();
            auto& tiles = b.tiles;
            for (size_t i = 0; i < tiles.size(); i++) {
                point dst = b.to_pos(i);
                if (tiles[i] == board::hallway && !g.at(dst)) {
                    targets.push_back(dst);
                }
            }
        }

        moves.clear();
        for (auto dst : targets) {
            if (dist(pl.pos, dst) * pl.cost <= threshold) {
                auto steps = pathfind(g, pl.pos, dst);
                if (steps > 0) {
                    moves.push_back(pl.move(dst, steps));
                }
            }
        }

        return moves;
    }

    int32_t simulate(game g) {
        int32_t best_so_far = INT32_MAX;
        size_t m = 0;
        size_t mm = 0;
        std::vector<game> stack{g};
        while (!stack.empty()) {
            m++;
            mm = std::max(mm, stack.size());
            auto curr = stack.back();
            stack.pop_back();
            if (curr.score() >= best_so_far)
                continue;

            for (auto& pl : curr.players) {
                for (auto hyp : possible_moves(curr, pl, best_so_far - pl.energy)) {
                    game b = curr;
                    b.players[hyp.id] = hyp;
                    if (b.finished()) {
                        if (b.score() < best_so_far) {
                            best_so_far = b.score();
                            std::cout << best_so_far << " " << curr.score() << " / " << b.score() << " " << m << "("
                                      << mm << ")"
                                      << "\n";
                        }
                    } else if (b.score() < best_so_far) {
                        stack.push_back(b);
                    }
                }
            }
            if (m % 1000000 == 0)
                std::cout << "-> " << m << "\n";
        }
        std::cout << "games " << m << "(" << mm << ")\n";

        return best_so_far;
    }
}

amp::game setup_1() {
    amp::game g;

    g._b = new amp::board;
    g.b().tile(0, 0) = amp::board::hallway;
    g.b().tile(1, 0) = amp::board::hallway;
    g.b().tile(3, 0) = amp::board::hallway;
    g.b().tile(5, 0) = amp::board::hallway;
    g.b().tile(7, 0) = amp::board::hallway;
    g.b().tile(9, 0) = amp::board::hallway;
    g.b().tile(10, 0) = amp::board::hallway;

    g.b().tile(2, 0) = amp::board::soft_wall;
    g.b().tile(4, 0) = amp::board::soft_wall;
    g.b().tile(6, 0) = amp::board::soft_wall;
    g.b().tile(8, 0) = amp::board::soft_wall;

    g.b().tile(2, 1) = amp::board::room;
    g.b().tile(2, 2) = amp::board::room;
    g.b().tile(4, 1) = amp::board::room;
    g.b().tile(4, 2) = amp::board::room;
    g.b().tile(6, 1) = amp::board::room;
    g.b().tile(6, 2) = amp::board::room;
    g.b().tile(8, 1) = amp::board::room;
    g.b().tile(8, 2) = amp::board::room;

    g.b().goals[0] = {{2, 2}, {2, 1}};
    g.b().goals[1] = {{4, 2}, {4, 1}};
    g.b().goals[2] = {{6, 2}, {6, 1}};
    g.b().goals[3] = {{8, 2}, {8, 1}};

    auto plA = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'A';
        pl.cost = 1;
        return pl;
    };

    auto plB = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'B';
        pl.cost = 10;
        return pl;
    };

    auto plC = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'C';
        pl.cost = 100;
        return pl;
    };

    auto plD = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'D';
        pl.cost = 1000;
        return pl;
    };

    g.players.resize(8);
    g.players[0] = plA(0, 2, 2);
    g.players[1] = plA(1, 8, 2);
    g.players[2] = plB(2, 2, 1);
    g.players[3] = plB(3, 6, 1);
    g.players[4] = plC(4, 4, 1);
    g.players[5] = plC(5, 6, 2);
    g.players[6] = plD(6, 4, 2);
    g.players[7] = plD(7, 8, 1);

    /* g.players[0].pos = {6, 2}; */
    /* g.players[1].pos = {8, 2}; */
    /* g.players[2].pos = {2, 1}; */
    /* g.players[3].pos = {8, 1}; */
    /* g.players[4].pos = {4, 1}; */
    /* g.players[5].pos = {6, 1}; */
    /* g.players[6].pos = {2, 2}; */
    /* g.players[7].pos = {4, 2}; */

    return g;
}

amp::game setup_2() {
    amp::game g;

    g._b = new amp::board;
    g.b().tile(0, 0) = amp::board::hallway;
    g.b().tile(1, 0) = amp::board::hallway;
    g.b().tile(3, 0) = amp::board::hallway;
    g.b().tile(5, 0) = amp::board::hallway;
    g.b().tile(7, 0) = amp::board::hallway;
    g.b().tile(9, 0) = amp::board::hallway;
    g.b().tile(10, 0) = amp::board::hallway;

    g.b().tile(2, 0) = amp::board::soft_wall;
    g.b().tile(4, 0) = amp::board::soft_wall;
    g.b().tile(6, 0) = amp::board::soft_wall;
    g.b().tile(8, 0) = amp::board::soft_wall;

    g.b().tile(2, 1) = amp::board::room;
    g.b().tile(2, 2) = amp::board::room;
    g.b().tile(2, 3) = amp::board::room;
    g.b().tile(2, 4) = amp::board::room;
    g.b().tile(4, 1) = amp::board::room;
    g.b().tile(4, 2) = amp::board::room;
    g.b().tile(4, 3) = amp::board::room;
    g.b().tile(4, 4) = amp::board::room;
    g.b().tile(6, 1) = amp::board::room;
    g.b().tile(6, 2) = amp::board::room;
    g.b().tile(6, 3) = amp::board::room;
    g.b().tile(6, 4) = amp::board::room;
    g.b().tile(8, 1) = amp::board::room;
    g.b().tile(8, 2) = amp::board::room;
    g.b().tile(8, 3) = amp::board::room;
    g.b().tile(8, 4) = amp::board::room;

    g.b().goals[0] = {{2, 4}, {2, 3}, {2, 2}, {2, 1}};
    g.b().goals[1] = {{4, 4}, {4, 3}, {4, 2}, {4, 1}};
    g.b().goals[2] = {{6, 4}, {6, 3}, {6, 2}, {6, 1}};
    g.b().goals[3] = {{8, 4}, {8, 3}, {8, 2}, {8, 1}};

    auto plA = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'A';
        pl.cost = 1;
        return pl;
    };

    auto plB = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'B';
        pl.cost = 10;
        return pl;
    };

    auto plC = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'C';
        pl.cost = 100;
        return pl;
    };

    auto plD = [](uint8_t id, int32_t x, int32_t y) {
        amp::player pl;
        pl.id = id;
        pl.pos = {x, y};
        pl.type = 'D';
        pl.cost = 1000;
        return pl;
    };

    g.players.resize(16);
    g.players[0] = plA(0, 2, 4);
    g.players[1] = plA(1, 8, 4);
    g.players[2] = plA(2, 6, 3);
    g.players[3] = plA(3, 8, 2);

    g.players[4] = plB(4, 2, 1);
    g.players[5] = plB(5, 4, 3);
    g.players[6] = plB(6, 6, 1);
    g.players[7] = plB(7, 6, 2);

    g.players[8] = plC(8, 4, 1);
    g.players[9] = plC(9, 4, 2);
    g.players[10] = plC(10, 6, 4);
    g.players[11] = plC(11, 8, 3);

    g.players[12] = plD(12, 2, 2);
    g.players[13] = plD(13, 2, 3);
    g.players[14] = plD(14, 4, 4);
    g.players[15] = plD(15, 8, 1);

    g.players[0].pos = {6, 3};
    g.players[1].pos = {6, 4};
    g.players[2].pos = {8, 2};
    g.players[3].pos = {8, 4};

    g.players[4].pos = {2, 1};
    g.players[5].pos = {4, 3};
    g.players[6].pos = {6, 2};
    g.players[7].pos = {8, 1};

    g.players[8].pos = {4, 1};
    g.players[9].pos = {4, 2};
    g.players[10].pos = {6, 1};
    g.players[11].pos = {8, 3};

    g.players[12].pos = {2, 2};
    g.players[13].pos = {2, 3};
    g.players[14].pos = {2, 4};
    g.players[15].pos = {4, 4};

    return g;
}

int main() {
    amp::game g = setup_2();
    std::cout << g << "\n";

    std::cout << simulate(g) << "\n";
}
